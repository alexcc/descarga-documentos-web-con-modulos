#!/usr/bin/python3

from cache import Cache

if __name__ == '__main__':
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.retrieve('https://www.aulavirtual.urjc.es/moodle/')
    c.retrieve('https://es.wikipedia.org/wiki/Wikipedia:Portada')
    c.show_all()

