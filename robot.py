import urllib.request
#!/usr/bin/python3
class Robot:

    def __init__(self, url):
        self.url = url
        self.retrieved = False
        print(self.url)

    def retrieve(self):
        if not self.retrieved:
            print("Descargarndo ...")
            f = urllib.request.urlopen(self.url)
            self.content = f.read().decode('utf-8')
            self.retrieved = True

    def content(self):
        self.retrieve()
        return self.content

    def show(self):
        print(self.content())

if __name__ == '__main__':
    r = Robot('http://gsyc.urjc.es/')
    print(r.url)
    r.show()
    r.retrieve()
